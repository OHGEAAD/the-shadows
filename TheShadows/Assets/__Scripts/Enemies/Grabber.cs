﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grabber : EnemyController
{
	[SerializeField]
	Vector3 target;
	[SerializeField]
	Animator anim;

	[SerializeField]
	float pursueRadius = 3;
	[SerializeField]
	float attackRadius = 1;
	[SerializeField]
	float patrolRadius = 5;
	[SerializeField]
	float patrolTime = 3;

	bool foundPlayer;
	float curPatrolTime;
	Rigidbody2D rb;
	Vector3 _velocity;
	EnemyAttributes enemyAttributes;

	private void Start()
	{
		rb = this.GetComponent<Rigidbody2D>();
		enemyAttributes = this.GetComponent<EnemyAttributes>();
		target = new Vector3(Random.Range(-patrolRadius, patrolRadius), Random.Range(-patrolRadius, patrolRadius));
	}

	void FixedUpdate()
    {
		CheckState();
		PerformAction();

		this.transform.position = this.transform.position + new Vector3(rb.velocity.x, rb.velocity.y);
	}

	public override void Attack()
	{
		
	}

	public override void Evade()
	{
		
	}

	public override void Idle()
	{
		
	}

	public override void Patrol()
	{
		if (curPatrolTime >= patrolTime)
		{
			target = new Vector3(Random.Range(-patrolRadius, patrolRadius), Random.Range(-patrolRadius, patrolRadius));
			curPatrolTime = 0;
		}
		curPatrolTime += Time.deltaTime;
		Pursue();
	}

	public override void Pursue()
	{
		rb.velocity = (target - this.transform.position).normalized * enemyAttributes.Speed;	
	}

	public override void CheckState()
	{
		if (!rb) return;

		Collider[] cols = Physics.OverlapSphere(this.transform.position, pursueRadius);
		foundPlayer = false;
		foreach (Collider col in cols)
		{
			if (col.gameObject.tag == "Player")
			{
				Debug.Log("found player");
				foundPlayer = true;
				this.target = col.gameObject.transform.position;
				break;
			}
		}

		float dist = Vector3.Distance(target, this.transform.position);
		if (dist > pursueRadius)
		{
			steeringBehavior = SteeringBehavior.PATROL;
		}
		else if (dist <= pursueRadius)
		{
			if (dist <= attackRadius)
			{
				if (foundPlayer)
				{
					steeringBehavior = SteeringBehavior.ATTACK;
				}
				else
				{
					curPatrolTime += patrolTime;
				}
				
			}
		}
	}

	public override void PerformAction()
	{
		switch (steeringBehavior)
		{
			case SteeringBehavior.IDLE:
				Idle();
				break;
			case SteeringBehavior.ATTACK:
				Attack();
				break;
			case SteeringBehavior.EVADE:
				Evade();
				break;
			case SteeringBehavior.PURSUE:
				Pursue();
				break;
			case SteeringBehavior.PATROL:
				Patrol();
				break;
		}
	}

	private void OnDrawGizmos()
	{
		Debug.Log("In OnDrawGizmos");
		Gizmos.color = Color.red;
		Gizmos.DrawSphere(this.transform.position, pursueRadius);
	}
}
