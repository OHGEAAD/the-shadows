﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAttributes : MonoBehaviour
{
	[SerializeField]
	string name;
	[SerializeField]
	int health;
	[SerializeField]
	int curHealth;
	[SerializeField]
	float speed;

	public string Name
	{
		get { return name; }
	}

	public int Health
	{
		get { return health; }
		
	}

	public int CurHealth
	{
		get { return curHealth; }
		set { curHealth = value; }
	}

	public float Speed
	{
		get { return speed; }
	}

}
