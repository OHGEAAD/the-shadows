﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum SteeringBehavior { IDLE, PATROL, PURSUE, ATTACK, EVADE };
public abstract class EnemyController : MonoBehaviour
{
	[SerializeField]
	protected SteeringBehavior steeringBehavior;

	public abstract void Patrol();
	public abstract void Pursue();
	public abstract void Attack();
	public abstract void Evade();
	public abstract void Idle();

	public abstract void PerformAction();
	public abstract void CheckState();

}
