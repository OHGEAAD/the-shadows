﻿using UnityEngine;

public class FollowCamera : MonoBehaviour
{
	[SerializeField]
	Transform target;

	[SerializeField]
	float smoothingSpeed = 0.125f;

	[SerializeField]
	Vector3 offset;

	private void LateUpdate()
	{
		Vector3 desiredPosition = target.position + offset;
		Vector3 smoothedPosition = Vector3.Lerp(transform.position, desiredPosition, smoothingSpeed);
		transform.position = smoothedPosition;
	}


}
